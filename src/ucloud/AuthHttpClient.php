<?php
namespace Yanan\Ufile\Ucloud;

class AuthHttpClient
{
    public $Auth;
    public $Type;
    public $MimeType;

    public function __construct($auth, $mimetype = null, $type = CheckStatus::HEAD_FIELD_CHECK)
    {
        $this->Type = $type;
        $this->MimeType = $mimetype;
        $makeAuth = new MakeAuth();
        $this->Auth = $makeAuth->UCloud_MakeAuth($auth);
    }

    //@results: ($resp, $error)
    public function RoundTrip($req)
    {
        if ($this->Type === CheckStatus::HEAD_FIELD_CHECK) {
            $token = $this->Auth->SignRequest($req, $this->MimeType, $this->Type);
            $req->Header['Authorization'] = $token;
        }
        $http = new Http();
        return $http->UCloud_Client_Do($req);
    }
}


