<?php
namespace Yanan\Ufile\Ucloud;

class Proxy{

    //------------------------------普通上传------------------------------
    function UCloud_PutFile($bucket, $key, $file)
    {
        $action_type = ActionType::PUTFILE;
        $ucloudUrl = new UcloudUrl();
        $err = $ucloudUrl->CheckConfig(ActionType::PUTFILE);
        if ($err != null) {
            return array(null, $err);
        }

        $f = @fopen($file, "r");
        if (!$f) return array(null, new UcloudError(-1, -1, "open $file error"));

        $UCLOUD_PROXY_SUFFIX = config('ufile.UCLOUD_PROXY_SUFFIX');
        $host = $bucket . $UCLOUD_PROXY_SUFFIX;
        $path = $key;
        $content  = @fread($f, filesize($file));
        $ucloudUrl = new UcloudUrl();
        list($mimetype, $err) = $ucloudUrl->GetFileMimeType($file);
        if ($err) {
            fclose($f);
            return array("", $err);
        }
        $req = new HttpRequest('PUT', array('host'=>$host, 'path'=>$path), $content, $bucket, $key, $action_type);
        $req->Header['Expect'] = '';
        $req->Header['Content-Type'] = $mimetype;

        $client = new AuthHttpClient(null, $mimetype);
        $http = new Http();
        list($data, $err) = $http->UCloud_Client_Call($client, $req);
        fclose($f);
        return array($data, $err);
    }

    //------------------------------表单上传------------------------------
    function UCloud_MultipartForm($bucket, $key, $file)
    {
        $action_type = ActionType::POSTFILE;
        $ucloudUrl = new UcloudUrl();
        $err = $ucloudUrl->CheckConfig(ActionType::POSTFILE);
        if ($err != null) {
            return array(null, $err);
        }

        $f = @fopen($file, "r");
        if (!$f) return array(null, new UcloudError(-1, -1, "open $file error"));

        $UCLOUD_PROXY_SUFFIX = config('ufile.UCLOUD_PROXY_SUFFIX');
        $host = $bucket . $UCLOUD_PROXY_SUFFIX;
        $path = "";
        $fsize = filesize($file);
        $content = "";
        if ($fsize != 0) {
            $content = @fread($f, filesize($file));
            if ($content == FALSE) {
                fclose($f);
                return array(null, new UcloudError(0, -1, "read file error"));
            }
        }
        $ucloudUrl = new UcloudUrl();
        list($mimetype, $err) = $ucloudUrl->GetFileMimeType($file);
        if ($err) {
            fclose($f);
            return array("", $err);
        }

        $req = new HttpRequest('POST', array('host'=>$host, 'path'=>$path), $content, $bucket, $key, $action_type);
        $req->Header['Expect'] = '';
        $token = new SignRequest(null, $req, $mimetype);

        $fields = array('Authorization'=>$token, 'FileName' => $key);
        $files  = array('files'=>array('file', $file, $content, $mimetype));

        $client = new AuthHttpClient(null, CheckStatus::NO_AUTH_CHECK);
        $http = new Http();
        list($data, $err) = $http->UCloud_Client_CallWithMultipartForm($client, $req, $fields, $files);
        fclose($f);
        return array($data, $err);
    }

    //------------------------------分片上传------------------------------
    function UCloud_MInit($bucket, $key)
    {
        $ucloudUrl = new UcloudUrl();
        $err = $ucloudUrl->CheckConfig(ActionType::MINIT);
        if ($err != null) {
            return array(null, $err);
        }

        $UCLOUD_PROXY_SUFFIX = config('ufile.UCLOUD_PROXY_SUFFIX');
        $host = $bucket . $UCLOUD_PROXY_SUFFIX;
        $path = $key;
        $querys = array(
            "uploads" => ""
        );
        $req = new HttpRequest('POST', array('host'=>$host, 'path'=>$path, 'query'=>$querys), null, $bucket, $key);
        $req->Header['Content-Type'] = 'application/x-www-form-urlencoded';

        $client = new AuthHttpClient(null);
        $http = new Http();
        return $http->UCloud_Client_Call($client, $req);
    }

    //@results: (tagList, err)
    function UCloud_MUpload($bucket, $key, $file, $uploadId, $blkSize, $partNumber=0)
    {
        $ucloudUrl = new UcloudUrl();
        $err = $ucloudUrl->CheckConfig(ActionType::MUPLOAD);
        if ($err != null) {
            return array(null, $err);
        }

        $f = @fopen($file, "r");
        if (!$f) return array(null, new UcloudError(-1, -1, "open $file error"));

        $UCLOUD_PROXY_SUFFIX = config('ufile.UCLOUD_PROXY_SUFFIX');

        $etagList = array();
        $ucloudUrl = new UcloudUrl();
        list($mimetype, $err) = $ucloudUrl->GetFileMimeType($file);
        if ($err) {
            fclose($f);
            return array("", $err);
        }
        $client   = new AuthHttpClient(null);
        for(;;) {
            $host = $bucket . $UCLOUD_PROXY_SUFFIX;
            $path = $key;
            if (@fseek($f, $blkSize*$partNumber, SEEK_SET) < 0) {
                fclose($f);
                return array(null, new UcloudError(0, -1, "fseek error"));
            }
            $content = @fread($f, $blkSize);
            if ($content == FALSE) {
                if (feof($f)) break;
                fclose($f);
                return array(null, new UcloudError(0, -1, "read file error"));
            }

            $querys = array(
                "uploadId" => $uploadId,
                "partNumber" => $partNumber
            );
            $req = new HttpRequest('PUT', array('host'=>$host, 'path'=>$path, 'query'=>$querys), $content, $bucket, $key);
            $req->Header['Content-Type'] = $mimetype;
            $req->Header['Expect'] = '';
            $http = new Http();
            list($data, $err) = $http->UCloud_Client_Call($client, $req);
            if ($err) {
                fclose($f);
                return array(null, $err);
            }
            $etag = @$data['ETag'];
            $part = @$data['PartNumber'];
            if ($part != $partNumber) {
                fclose($f);
                return array(null, new UcloudError(0, -1, "unmatch partnumber"));
            }
            $etagList[] = $etag;
            $partNumber += 1;
        }
        fclose($f);
        return array($etagList, null);
    }

    function UCloud_MFinish($bucket, $key, $uploadId, $etagList, $newKey = '')
    {
        $ucloudUrl = new UcloudUrl();
        $err = $ucloudUrl->CheckConfig(ActionType::MFINISH);
        if ($err != null) {
            return array(null, $err);
        }

        $UCLOUD_PROXY_SUFFIX = config('ufile.UCLOUD_PROXY_SUFFIX');
        $host = $bucket . $UCLOUD_PROXY_SUFFIX;
        $path = $key;
        $querys = array(
            'uploadId' => $uploadId,
            'newKey' => $newKey,
        );

        $body = @implode(',', $etagList);
        $req = new HttpRequest('POST', array('host'=>$host, 'path'=>$path, 'query'=>$querys), $body, $bucket, $key);
        $req->Header['Content-Type'] = 'text/plain';

        $client = new AuthHttpClient(null);
        $http = new Http();
        return $http->UCloud_Client_Call($client, $req);
    }

    function UCloud_MCancel($bucket, $key, $uploadId)
    {
        $ucloudUrl = new UcloudUrl();
        $err = $ucloudUrl->CheckConfig(ActionType::MCANCEL);
        if ($err != null) {
            return array(null, $err);
        }

        $UCLOUD_PROXY_SUFFIX = config('ufile.UCLOUD_PROXY_SUFFIX');
        $host = $bucket . $UCLOUD_PROXY_SUFFIX;
        $path = $key;
        $querys = array(
            'uploadId' => $uploadId
        );

        $req = new HTTP_Request('DELETE', array('host'=>$host, 'path'=>$path, 'query'=>$querys), null, $bucket, $key);
        $req->Header['Content-Type'] = 'application/x-www-form-urlencoded';

        $client = new AuthHttpClient(null);
        $http = new Http();
        return $http->UCloud_Client_Call($client, $req);
    }

    //------------------------------秒传------------------------------
    function UCloud_UploadHit($bucket, $key, $file)
    {
        $ucloudUrl = new UcloudUrl();
        $err = $ucloudUrl->CheckConfig(ActionType::UPLOADHIT);
        if ($err != null) {
            return array(null, $err);
        }

        $f = @fopen($file, "r");
        if (!$f) return array(null, new UcloudError(-1, -1, "open $file error"));

        $content = "";
        $fileSize = filesize($file);
        if ($fileSize != 0) {
            $content  = @fread($f, $fileSize);
            if ($content == FALSE) {
                fclose($f);
                return array(null, new UcloudError(0, -1, "read file error"));
            }
        }
        $ucloudUrl = new UcloudUrl();
        list($fileHash, $err) = $ucloudUrl->UCloud_FileHash($file);
        if ($err) {
            fclose($f);
            return array(null, $err);
        }
        fclose($f);

        $UCLOUD_PROXY_SUFFIX = config('ufile.UCLOUD_PROXY_SUFFIX');
        $host = $bucket . $UCLOUD_PROXY_SUFFIX;
        $path = "uploadhit";
        $querys = array(
            'Hash' => $fileHash,
            'FileName' => $key,
            'FileSize' => $fileSize
        );

        $req = new HttpRequest('POST', array('host'=>$host, 'path'=>$path, 'query'=>$querys), null, $bucket, $key);
        $req->Header['Content-Type'] = 'application/x-www-form-urlencoded';

        $client = new AuthHttpClient(null);
        $http = new Http();
        return $http->UCloud_Client_Call($client, $req);
    }

    //------------------------------删除文件------------------------------
    function UCloud_Delete($bucket, $key)
    {
        $ucloudUrl = new UcloudUrl();
        $err = $ucloudUrl->CheckConfig(ActionType::DELETE);
        if ($err != null) {
            return array(null, $err);
        }

        $UCLOUD_PROXY_SUFFIX = config('ufile.UCLOUD_PROXY_SUFFIX');
        $host = $bucket . $UCLOUD_PROXY_SUFFIX;
        $path = "$key";

        $req = new HttpRequest('DELETE', array('host'=>$host, 'path'=>$path), null, $bucket, $key);
        $req->Header['Content-Type'] = 'application/x-www-form-urlencoded';

        $client = new AuthHttpClient(null);
        $http = new Http();
        return $http->UCloud_Client_Call($client, $req);
    }

    //------------------------------生成公有文件Url------------------------------
    // @results: $url
    function UCloud_MakePublicUrl($bucket, $key)
    {
        $UCLOUD_PROXY_SUFFIX = config('ufile.UCLOUD_PROXY_SUFFIX');
        return $bucket . $UCLOUD_PROXY_SUFFIX . "/" . rawurlencode($key);
    }
    //------------------------------生成私有文件Url------------------------------
    // @results: $url
    function UCloud_MakePrivateUrl($bucket, $key, $expires = 0)
    {
        $ucloudUrl = new UcloudUrl();
        $err = $ucloudUrl->CheckConfig(ActionType::GETFILE);
        if ($err != null) {
            return array(null, $err);
        }

        $UCLOUD_PUBLIC_KEY = config('ufile.UCLOUD_PUBLIC_KEY');

        $public_url = $this->UCloud_MakePublicUrl($bucket, $key);
        $req = new HttpRequest('GET', array('path'=>$public_url), null, $bucket, $key);
        if ($expires > 0) {
            $req->Header['Expires'] = $expires;
        }

        $client = new AuthHttpClient(null);
        $temp = $client->Auth->SignRequest($req, null, CheckStatus::QUERY_STRING_CHECK);
        $signature = substr($temp, -28, 28);
        $url = $public_url . "?UCloudPublicKey=" . rawurlencode($UCLOUD_PUBLIC_KEY) . "&Signature=" . rawurlencode($signature);
        if ('' != $expires) {
            $url .= "&Expires=" . rawurlencode($expires);
        }
        return $url;
    }

}
