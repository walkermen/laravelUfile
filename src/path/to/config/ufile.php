<?php
 return array(

     /*
      * SDK 版本号
      * */

     'SDK_VER' => '1.0.8',

     /*
      * 空间域名后缀
      * */

     'UCLOUD_PROXY_SUFFIX' => '.ufile.ucloud.cn',


     /*
      * 公钥
      * */

     'UCLOUD_PUBLIC_KEY' => '',


     /*
      * 私钥
      * */

     'UCLOUD_PRIVATE_KEY' => '',

     /*
      * 存储空间
      * */

     'bucket' => '',

 );