# laravelUfile

#### 项目介绍
laravel 云上传

#### 软件架构
laravel 5.2


#### 安装教程

1. composer require yanan/ufile:dev-master

2. 然后添加服务： 
   在 config/app.php添加 
   Yanan\Ufile\UpFileProvider::class, 
   
   和别名的配置： 
   'ufile' => Yanan\Ufile\UpFileProvider::class, 

3. composer dump-autoload 

4.  php artisan vendor:publish –provider=”Yanan\Ufile\UpFileProvider” 
   

#### 使用说明

1. //执行上传
   $ufile = new UpFile();
   $put_res = $ufile->putFile($bucket, $key, $res_filename);
   
2. //下载文件
   $ufile = new UpFile();
   $get_res = $ufile->getFile($bucket,$key,$type='public')




